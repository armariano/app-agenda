package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Agenda.inicializarLista();
        int sair = 9;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("================================ AGENDA DE CONTATOS ================================");
            System.out.println("1 - Incluir contato | 2 - Buscar contato | 3 - Listar contatos | 4 - Remover contato");
            System.out.println("Digite um número conforme opções acima: ");
            System.out.println("====================================================================================");
            Motor.processar(scanner.nextInt());
        } while (sair == 9);
    }
}
