package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

    private static ArrayList<Pessoa> listaPessoas = new ArrayList<Pessoa>();

    public Agenda() {
    }

    public void incluirContato(Pessoa pessoa) {
        listaPessoas.add(pessoa);
    }

    public void removerContato(String filtro, TipoBusca tipo) {
        Pessoa pessoa = new Pessoa();
        for (Pessoa pes : listaPessoas) {
            if (tipo.equals(TipoBusca.EMAIL)) {
                if (filtro.equals(pes.getEmail())) {
                    pessoa = pes;
                }
            }
            if(tipo.equals(TipoBusca.NUMERO)) {
                if(filtro.equals(pes.getNumeroContato())){
                    pessoa = pes;
                }
            }
        }
        if(!pessoa.getNome().equals(null)) {
            listaPessoas.remove(pessoa);
            System.out.println(pessoa.getNome() + " foi removido da sua lista de contatos.");
        }
    }

    public Pessoa buscarContato(String filtro, TipoBusca tipo) {
        Pessoa pessoa = new Pessoa();

        for (Pessoa pes : listaPessoas) {
            if (tipo.equals(TipoBusca.EMAIL)) {
                if (filtro.equals(pes.getEmail())) {
                    pessoa = pes;
                }
            }
            if(tipo.equals(TipoBusca.NUMERO)){
                if(filtro.equals(pes.getNumeroContato())){
                    pessoa = pes;
                }
            }
        }
        return pessoa;
    }

    public static void inicializarLista(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Alder");
        pessoa.setNumeroContato("11223344");
        pessoa.setEmail("teste@teste.com.br");
        listaPessoas.add((pessoa));

        pessoa = new Pessoa();
        pessoa.setNome("João");
        pessoa.setNumeroContato("99887766");
        pessoa.setEmail("teste01@teste.com.br");
        listaPessoas.add((pessoa));

        pessoa = new Pessoa();
        pessoa.setNome("Maria");
        pessoa.setNumeroContato("55446655");
        pessoa.setEmail("teste02@teste.com.br");
        listaPessoas.add((pessoa));
    }

    public static ArrayList<Pessoa> getListaPessoas() {
        return listaPessoas;
    }
}
