package br.com.itau;

public class Pessoa {

    private String nome;
    private String email;
    private String numeroContato;

    public Pessoa(){}

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getNumeroContato() {
        return numeroContato;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumeroContato(String numeroContato) {
        this.numeroContato = numeroContato;
    }
}
