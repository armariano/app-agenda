package br.com.itau;

import java.util.Scanner;

public class Motor {

    public static void processar(int opcao){

        if(opcao == 1){
            processarInclusao();
            return;
        }
        if(opcao == 2){
            processarBusca();
            return;
        }
        if(opcao == 3) {
            processarExibicao();
            return;
        }
        if(opcao == 4) {
            processarRemocao();
            return;
        }else{
            System.out.println("Opção inválida.");
        }
    }

    public static void processarInclusao(){
        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();
        int quantidadeContato = 0;

        System.out.println("Quantos contatos deseja inserir: ");
        quantidadeContato = scanner.nextInt();

        for (int i = 1; i <= quantidadeContato; i++) {
            Pessoa pessoa = new Pessoa();

            System.out.println("Contato " + i);
            System.out.println("Nome: ");
            pessoa.setNome(scanner.next());

            System.out.println("Número: ");
            pessoa.setNumeroContato(scanner.next());

            System.out.println("Email: ");
            pessoa.setEmail(scanner.next());

            agenda.incluirContato(pessoa);

            System.out.println("Contato incluido com sucesso.");
            System.out.println("");
        }
    }

    public static void processarBusca(){
        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();
        Pessoa pessoa = new Pessoa();
        int opcao;
        String email;
        String numero;

        System.out.println("Insira a opção de filtro para busca");
        System.out.println("1 - Email | 2 - Número");
        opcao = scanner.nextInt();

        System.out.println("");
        if(opcao == 1){
            System.out.println("Insira o e-mail: ");
            email = scanner.next();
            pessoa = agenda.buscarContato(email, TipoBusca.EMAIL);
        }else{
            System.out.println("Insira o numero: ");
            numero = scanner.next();
            pessoa = agenda.buscarContato(numero, TipoBusca.NUMERO);
        }

        System.out.println("");
        if(!pessoa.getNome().equals(null)){
            System.out.println("Contato encontrado:");
            System.out.println("Nome: " + pessoa.getNome());
            System.out.println("Numero: " + pessoa.getNumeroContato());
            System.out.println("Email: " + pessoa.getEmail());
        }else{
            System.out.println("Contato não encontrado.");
        }


    }

    public static void processarRemocao(){
        int opcao;
        String email;
        String numero;
        Agenda agenda = new Agenda();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Insira a opção de filtro para remover o contato");
        System.out.println("1 - Email | 2 - Número");
        opcao = scanner.nextInt();

        System.out.println("");
        if(opcao == 1){
            System.out.println("Insira o e-mail: ");
            email = scanner.next();
            agenda.removerContato(email, TipoBusca.EMAIL);
        }else{
            System.out.println("Insira o numero: ");
            numero = scanner.next();
            agenda.removerContato(numero, TipoBusca.NUMERO);
        }
    }

    public static void processarExibicao(){
        for (Pessoa pessoa: Agenda.getListaPessoas()) {
            System.out.println(pessoa.getNome() + " - " + pessoa.getNumeroContato() + " - " + pessoa.getEmail());
            System.out.println("");
        }
    }
}
